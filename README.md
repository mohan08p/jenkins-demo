
#### Jenkins Demo

## Building Docker Images using Jenkins

1. **Launch Jenkins**,
Launch Jenkins as a Docker Container with the following command:

    docker run -d -u root --name jenkins -p 8080:8080 -p 50000:50000 -v /root/jenkins_2112:/var/jenkins_home jenkins/jenkins:2.112-alpine

All plugins and configurations get persisted to the host `(ssh root@host01)` at _/root/jenkins2112. Port 8080 opens the web dashboard, 50000 is used to communicate with other Jenkins agents. Finally, the image has an alpine base to reduce the size footprint.

2. **Load dashboard** and login to Jenkins on your own system, 
   the password can be found via,

    docker exec -it Jenkins cat /var/jenkins_home/secrets/initialAdminPassword

It may take a couple of seconds for Jenkins to finish starting and be available. In the next steps, you'll use the dashboard to configure the plugins and start building Docker Images.

3. **Configure docker plugin**

   The first step is to configure the Docker plugin. The plugin is based on a Jenkins Cloud plugin. When a build requires Docker, it will create a "Cloud Agent" via the plugin. The agent will be a Docker Container configured to talk to our Docker Daemon.

#### Task: Install Plugin
i) Within the Dashboard, select Manage Jenkins on the left.
ii) On the Configuration page, select Manage Plugins.
iii) Manage Plugins page will give you a tabbed interface. Click Available to view all the Jenkins plugins that can be installed.
iv) Using the search box, search for Docker. There are multiple Docker plugins, select Docker using the checkbox under the Cloud Providers header.
v) Click Install without Restart at the bottom.
vi) The plugins will now be downloaded and installed. Once complete, click the link Go back to the top page.

4. **Add docker agent**

   Once the plugins have been installed, you can configure how they launch the Docker Containers. The configuration will tell the plugin which Docker Image to use for the agent and which Docker daemon to run the containers and builds on.
   
#### Task: Configure Plugin
This step configures the plugin to communicate with a Docker host/daemon.

i) Once again, select Manage Jenkins.
ii) Select Configure System to access the main Jenkins settings.
iii) At the bottom, there is a dropdown called Add a new cloud. Select Docker from the list.
iv) The Docker Host URI is where Jenkins launches the agent container. In this case, we'll use the same daemon as running Jenkins, but you could split the two for scaling. Enter the URL tcp://172.17.0.43:2345
v) Use Test Connection to verify Jenkins can talk to the Docker Daemon. You should see the Docker version number returned.

The Host IP address is the IP of your build agent / Docker Host.

## Task: Configure Docker Agent Template
The Docker Agent Template is the Container which will be started to handle your build process.

i) Click Docker Agent templates... and then Add Docker Template. You can now configure the container options.

ii) Set the label of the agent to `docker-agent`. This is used by the Jenkins builds to indicate it should be built via the Docker Agent we're defining.

iii) For the Docker Image, use `benhall/dind-jenkins-agent:v2`. This image is configured with a Docker client and available at https://hub.docker.com/r/benhall/dind-jenkins-agent/

iv) Under Container Settings, In the "Volumes" text box enter /var/run/docker.sock:/var/run/docker.sock. This allows our build container to communicate with the host.

v) For Connect Method select Connect with SSH. The image is based on the Jenkins SSH Slave image meaning the default Inject SSH key will handle the authenication.

vi) Make sure it is Enabled.

vii) Click Save.

5. **Create a build project**

   This step creates a new project which Jenkins will build via our new agent. The project source code is at https://github.com/mohan08/jenkins-demo. The repository has a Dockerfile; this defines the instructions on how to produce the Docker Image. Jenkins doesn't need to know the details of how our project is built.Name as Jenkins DemoSet the label expression as docker-agent
   
   Run as shell command,
   
    ls
    docker info
    docker build -t mohan08p/jenkins-demo:${BUILD_NUMBER} .
    docker tag mohan08p/jenkins-demo:${BUILD_NUMBER}
    mohan08p/jenkins-demo:latest
    docker images

6. **Build Project**

We now have a configured job that will build Docker Images based on our Git repository. The next stage is to test and try it.

Also, you can see the progress using,
    
    docker logs --tail=10 jenkins

7. **View Console Output**

   Once the build has completed you should see the Image and Tags using the Docker CLI `docker image ls`.

   What was built into the Docker Image was a small HTTP server. You can launch it using: 
   
    docker run -d -p 80:80 mohan08p/jenkins-demo:latest

   Using cURL you should see the server respond: `curl host01`

For the deployment you can leverage the helm charts. 